go mod download;
go get -u ./;
fyne-cross linux -icon Icon.png -arch=* -name "go-static" -app-version 1.0;
fyne-cross windows -icon Icon.png -arch=* -name "go-static.exe" -app-version 1.0;
fyne-cross freebsd -icon Icon.png -arch=* -name "go-static" -app-version 1.0;


