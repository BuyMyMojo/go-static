package main

import (
	"flag"
	"image"
	"image/color"
	"math/rand"
	"os"
	"runtime"
	"sync"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/canvas"
)

func main() {
	// create a new application
	a := app.New()
	// create a new window
	w := a.NewWindow("go-noise")

	// Set flags
	width := flag.Int("w", 1024, "Width of the noise pattern.")
	height := flag.Int("h", 1024, "Height of the noise pattern.")
	threads := flag.Int("t", 4, "Set number of threads to use.")
	flag.Parse()

	// Set threads
	runtime.GOMAXPROCS(*threads)

	// Create image
	upLeft := image.Point{0, 0}
	lowRight := image.Point{*width, *height}
	img := image.NewGray(image.Rectangle{upLeft, lowRight})

	// Create canvis and set to noise
	canvas := w.Canvas()
	canvas.SetContent(generateStatic(*width, *height, img))

	// Set window size
	w.Resize(fyne.NewSize(float32(*width), float32(*height)))

	// Start updating the noise
	go updateCanvas(canvas, *width, *height, img)

	// Disable window resizing
	w.SetFixedSize(true)

	// Show window
	w.ShowAndRun()

	// Exit if window is closed
	os.Exit(3)

}

// updateCanvas updates the noise pattern.
func updateCanvas(canv fyne.Canvas, width int, height int, img image.Image) {

	// loop forever
	for i := 1; i > 0; i++ {
		// Create WaitGroup
		var wg sync.WaitGroup

		// Set color for each pixel.
		// Loop through every X pixel
		for x := 0; x < width; x++ {
			// Add threat to WaitGroup
			wg.Add(1)

			// Create goroutine that lopops through every Y pixel
			go func(x int) {
				for y := 0; y < height; y++ {
					// Generate random number
					BW := rand.Intn(2)

					// Set color for each pixel based on BW
					switch {
					case BW == 1:
						// Make pixel white
						img.(*image.Gray).Set(x, y, color.White)
					case BW == 0:
						// Make pixel black
						img.(*image.Gray).Set(x, y, color.Black)
					default:
						// Use zero value.
						img.(*image.Gray).Set(x, y, color.Black)
					}
				}
				// Decrement WaitGroup
				defer wg.Done()
			}(x)
		}

		// Wait for all goroutines to finish
		wg.Wait()

		// Update canvas
		canv.SetContent(canvas.NewImageFromImage(img))
	}
}

// Generate first static image.
func generateStatic(width int, height int, img image.Image) *canvas.Image {
	// Create WaitGroup
	var wg sync.WaitGroup

	// Set color for each pixel.
	// Loop through every X pixel
	for x := 0; x < width; x++ {
		// Add threat to WaitGroup
		wg.Add(1)

		// Create goroutine that lopops through every Y pixel
		go func(x int) {
			for y := 0; y < height; y++ {
				// Generate random number
				BW := rand.Intn(2)

				// Set color for each pixel based on BW
				switch {
				case BW == 1:
					// Make pixel white
					img.(*image.Gray).Set(x, y, color.White)
				case BW == 0:
					// Make pixel black
					img.(*image.Gray).Set(x, y, color.Black)
				default:
					// Use zero value.
					img.(*image.Gray).Set(x, y, color.Black)
				}
			}
			// Decrement WaitGroup
			defer wg.Done()
		}(x)
	}

	canvImg := canvas.NewImageFromImage(img)

	return canvImg
}
