
# go-static

A basic program that displays a random updating noise pattern


## License

[GPL-2.0](https://gitlab.com/BuyMyMojo/go-static/-/blob/main/LICENSE)


## Acknowledgements

 - [fyne](https://github.com/fyne-io/fyne)
 - [readme.so](https://readme.so/editor)


## Demo

![Demo gif](https://gitlab.com/BuyMyMojo/go-static/-/raw/main/images/demo.gif)


## Features

- Variable width and height
- Ability to limit multithreading


## Usage/Examples

```bash
./go-static -w <width in pixels (defaults to 1024)> -h <height in pixels (defaults to 1024)> -t <threads (defaults to 4)>
```


## FAQ

#### Why did I make this?

To learn Fyne

#### Is this useful?

Not exactly, I use it with OBS to have a static image to test recording


## Optimizations

Basically none!

I've made the static generation multi threaded but it's nothing professional